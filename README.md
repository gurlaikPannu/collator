# Collator #
TF-IDF implementation along with Cosine similarity for text matching

# DEPENDENCIES #
requirements.txt has all dependencies listed

# SETTING UP #
Run "sudo python setup.py install" to install all dependencies. If setup.py fails, install dependencies them manually listed in requirements.txt, using pip install or other package managers of your preference

# Usage #
### All scripts run from base folder (collator). Instructions are as follows: ###
1. cd collator/
2. chmod 755 bin/main.bash
3. ./bin/main.bash --help (this will display all the command line options available)

# TESTS #
1. Run them from base folder collator
2. python integration_test/test.py