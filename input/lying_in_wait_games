Carry the fight to the Rebellion’s treasonous scum, no matter where they attempt to hide…

Fantasy Flight Games is proud to announce the upcoming release of the Imperial Assault Carrier Expansion Pack for X-Wing™!

Designed for use within the game’s Cinematic Play and Epic Play formats, the Imperial Assault Carrier Expansion Pack contains one Gozanti-class cruiser miniature, two TIE fighter miniatures with a variant paint scheme, eleven ship cards, twenty-six upgrade cards, and all the maneuver dials, damage decks, tokens, and game pieces that you need to fly your Gozanti cruiser and its escort fighters. Additionally, the expansion includes a set of four new missions for Cinematic Play that follow a Gozanti charged with interdicting and destroying agents of the Rebel Alliance.

The Gozanti-class Cruiser

A highly versatile and heavily armed starship, the Gozanti-class cruiser could function as either a freighter or a cruiser and was used by various organizations across the galaxy. The Empire made extensive use of the Gozanti, though their cruisers featured heavier armor, faster engines, and better weapons than standard versions. Additionally, Imperial engineers added docking clamps to the ship’s design that allowed it to carry a complement of escort fighters.

Too large for the game’s Standard Play format, the Gozanti-class cruiser nonetheless enters X-Wing as a huge-base ship sculpted at the game’s standard 1/270 scale. Accordingly, it towers over the game’s small and large ships, even as it can carry and deploy up to four TIEs from its signature docking clamps.

The Imperial Assault Carrier Expansion Pack comes with everything you need to add one Gozanti-class cruiser and two TIE fighters to your games. You’ll find the Gozanti’s ship card , as well as ten TIE fighter ship cards, including four new unique Imperial aces. You’ll also find rules for deploying TIEs from your Gozanti, and you’ll find a wide variety of upgrades, including modifications that introduce new dynamics to Epic Play, plus ISB officer Agent Kallus , one of the primary villains from Star Wars Rebels.

Finally, the Imperial Assault Carrier Expansion Pack introduces a new campaign to X-Wing Cinematic Play. The four missions of Imperial Crackdown highlight the Gozanti as it pits Imperials and Rebels against each other in a series of battles for control of a distant sector of the galaxy.

The Imperial Assault Carrier Expansion Pack and Standard Play

Even as the expansion’s Gozanti miniature and its Huge ship only upgrades add new muscle to the game’s Epic Play format, the Imperial Assault Carrier Expansion Pack brings additional depth to Standard Play with its new TIE fighters, their pilots, and Agent Kallus.

A recurring antagonist in Star Wars Rebels, Agent Kallus is a cunning and relentless ISB officer who’s perfectly willing to join the front lines of any effort to crush the Empire’s enemies. In X-Wing, Agent Kallus appears as a crew upgrade who targets a single small or large ship at the start of the first round and then grants you an ongoing ability to change one of your  results to a   or   result.

At a cost of two squad points, Agent Kallus offers Imperial fleets an efficient alternative to upgrades like Push the Limit and Experimental Interface that grant extra actions. While his ability doesn’t allow you to change all your  results into more positive results as if you had spent a focus token, Agent Kallus functions both on offense and defense against the targeted ship, and his efficiency only improves as the game goes on, especially if he targets an extremely durable starship like the Millennium Falcon.
  
The Imperial Assault Carrier Expansion Pack includes two TIE fighters with an alternate blue paint scheme.

Meanwhile, the expansion’s two new TIE fighter miniatures feature the same coloration as the TIEs shown flying at the Battle of Endor in Return of the Jedi.

Moreover, with unique, new ace pilots like "Scourge," the Imperial Assault Carrier Expansion Pack invites you to explore new pilot combinations and further customize your Imperial fighter wings. A predator whose ferocity increases as he catches the scent of blood, “Scourge” gains an additional attack die whenever he attacks a ship that has already received at least one damage card. His pilot ability makes him an excellent pilot to partner with another ace with the same or higher pilot skill value, and it makes him an excellent recruit if you anticipate that you’ll face durable ships like the YT-1300 or Y-wing that can take a lot of damage before they explode. More importantly, though, his pilot ability makes him worth considering in any Imperial squadron that can take advantage of synergistic pilot abilities. You need your other pilots to do their jobs in order to get the best out of “Scourge,” but he bolsters their impact as well.

Allow No Rebels to Escape You

As its Gozanti-class cruiser allows you to deploy your TIEs directly into battle, the Imperial Assault Carrier Expansion Pack grants you startling new measures of tactical depth in X-Wing Cinematic and Epic Play. Additionally, its upgrades enhance the versatility of all X-Wing huge ships, and its four new ace TIE pilots lend their unique abilities to the game’s Standard Play format.

Pursue the Rebellion’s subversive pilots wherever they may flee. Hunt them down, and crush them. Allow no Rebels to escape you. The Imperial Assault Carrier Expansion Pack is scheduled to arrive at retailers in the fourth quarter of 2015. Until then, keep your eyes on our website for more previews and other X-Wing news!