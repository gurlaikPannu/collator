from sys import path
path.append('lib/')
import collator as col


def size_mb(docs):
    return sum(len(s.encode('utf-8')) for s in docs) / 1e6


def execution_speed_test(test_data, collator, train_tfidf_matrix):
    new_tfidf_matrix = collator.transform_fresh_data(test_data)
    for vector in new_tfidf_matrix:
        cosine_similarities_value = collator.cosine_similarity_value(vector, train_tfidf_matrix)


if __name__ == '__main__':
    from sklearn.datasets import fetch_20newsgroups
    from time import time
    import random
    import timeit

    data_train = fetch_20newsgroups(subset='train', shuffle=True, random_state=42, 
                                   remove=('headers', 'footers', 'quotes'))
    data_test = fetch_20newsgroups(subset='test', shuffle=True, random_state=42, 
                                   remove=('headers', 'footers', 'quotes'))

    data_train_size_mb = size_mb(data_train.data)
    print "{0} documents - {1:.3f}MB (train set)\n".format(len(data_train.data), data_train_size_mb)
    data_test_size_mb = size_mb(data_test.data)
    print "{0} documents - {1:.3f}MB (test set)\n".format(len(data_test.data), data_test_size_mb)

    print "Extracting features from the training dataset using a sparse vectorizer"
    t0 = time()
    collator = col.Collator(ngram_range=(1, 3))
    train_tfidf_matrix = collator.get_tfidf_matrix(data_train.data)
    duration = time() - t0
    print "Done in {0}s at {1:.3f}MB/s".format(duration, data_train_size_mb / duration)
    print "n_samples: {0}, n_features: {1}\n".format(*train_tfidf_matrix.shape)

    print 'Testing with 1 random article for {0} articles'.format(len(data_test.data))
    random_article_num = random.randint(0, len(data_test.data))
    test_data = data_test.data[random_article_num - 1: random_article_num]
    timer = timeit.Timer(lambda: execution_speed_test(test_data, collator, train_tfidf_matrix))
    num = 100
    total_time = timer.timeit(number=num)
    print 'Avg time: {0}'.format(total_time / num)