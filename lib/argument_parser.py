import argparse


def parse_arguments():
	parser = argparse.ArgumentParser(description='TF-IDF implemetations')

	parser.add_argument('-n', '--ngram_range', type=int, nargs=2, default=(1, 3), help='The lower and upper boundary of the range of n-values for different n-grams to be extracted. All values of n such that min_n <= n <= max_n will be used. Default -> (1, 3)')
	parser.add_argument('-r', '--matrix_range', type=int, nargs=2, default=(), help='Range of documents for which the cosine similarity matrix will be returned, where both lower and upper ranges are included')
	parser.add_argument('-s', '--similarity', type=int, nargs='*', default=(), help='Poition of the document for which to compute cosine similarity with all other documents. If position of other document is provided, then only cosine similarity between the documents is returned')

	return parser.parse_args()
