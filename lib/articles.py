EDITED_SAME_SOURCE = [
    'an_exceptional_planetary_system_discovered_in_cassiopeia_science', 
    'an_exceptional_planetary_system_discovered_in_cassiopeia_science_dup',
    'beijing_defeats_almaty_in_bid_to_host_2022_winter_olympics_sports',
    'beijing_defeats_almaty_in_bid_to_host_2022_winter_olympics_sports_dup',
    'clinton_income_doubled_since_2010_tax_docs_show_politics',
    'clinton_income_doubled_since_2010_tax_docs_show_politics_dup',
    'ouch_tori_spellings_benihana_burn_still_looks_super_painful_nearly_4_months_after_injury_entertainment',
    'ouch_tori_spellings_benihana_burn_still_looks_super_painful_nearly_4_months_after_injury_entertainment_dup',
    'lying_in_wait_games',
    'lying_in_wait_games_dup'
]


SAME_ARTICLE_DIFFERENT_SOURCE = [
    'stars_in_milky_way_have_moved_science',
    'milky_way_map_shows_many_stars_have_moved_out_from_place_of_birth_science',
    'filthy_rio_de_janeiro_water_a_threat_at_2016_olympics_sports',
    'rios_waters_are_so_filthy_that_2016_olympians_risk_becoming_violently_ill_and_unable_to_compete_sports',
    'trump_staffer_fired_following_report_of_racially_charged_facebook_posts_politics',
    'trump_campaign_adviser_fired_for_making_racially_charged_facebook_posts_politics',
    'travi$_scott_arrested_at_lollapalooza_2015_after_allegedly_telling_fans_to_jump_the_barricades_entertainment',
    'rapper_travi$_scott_is_arrested_after_encouraging_fans_to_jump_over_barricades_during_his_performance_at_lollapalooza_entertainment',
    'greek_shares_set_to_plunge_20%_as_stock_exchange_reopens_business',
    'greek_stocks_brace_for_plunge_as_athens_stock_exchange_to_reopen_for_first_time_in_a_month_business'
]


SAME_CATEGORY = [
    'soulcycle_ipo_5_things_to_know_about_the_boutique_fitness_chain_going_public_business',
    'bitcoin_exchange_mtgox_former_ceo_mark_karpeles_arrested_in_japan_business',
    'hedge_fund_risks_may_be_bigger_than_regulators_know_despite_dodd-frank_business',
    'dow_jones_industrial_average_drops_as_exxon_mobil_corporation_(xom)_and_chevron_corporation_(cvx)_sink_4%_business',
    'earnings_buzz_twitter_inc_(twtr)_facebook_inc_(fb)_linkedin_corp_(lnkd)_business',
    'dow_jones_industrial_average_rebounds_as_investors_shake_off_china_fears_ahead_of_federal_reserve_statement_business',
    'dow_jones_industrial_average_sinks_100_points_after_chinese_stocks_tumble_8.5%_in _shanghai_index_business',
    'whole_foods_market_inc_(wfm)_stock_price_plunges_12%_after_nyc_overcharging_scandal_hits_q3_sales_business',
    'walt_disney_company_(dis)_earnings_preview_q3_2015_profits_rise_and_the_future_is_bright_despite_tomorrowland_flop_business',
    'asian_stocks_slip_ahead_of_final_caixin_pmi_soft_energy_prices_weigh_business'
]


RANDOM = [
    'macau_as_vip_gamers_vanish_can_chinas_casino_haven_reinvent_itself_as_a_family-friendly_destination_companies',
    'election_2016_seeking_afl-cio_endorsement_sanders_clinton_offer_different_visions_to_top_labor_leaders_politics',
    'this_star_cluster_is_not_what_it_seems_messier_54_shows_lithium_problem_also_applies_outside_our_galaxy_science',
    'holy_hotness_shirtless_stephen_amell_and_jared_padalecki_display_their_six-pack_abs_for_a_good_cause_entertainment',
    'microsoft_corporations_windows_10_kicks_off_to_strong_start_as_usage_share_leaps_four_times_in_three_days_technology',
    'robotic_insect_mimics_natures_extreme_moves_science',
    'american_pharoah_wins_haskell_invitational_sports',
    'mh370_debris_flaperon_came_from_boeing_777_aircraft_and_was_not_a_spare_part_aviation_expert_says_politics',
    'affordable_care_act_unpaid_ospital_bills_are_shrinking_thanks_to_obamacare_companies',
    'with_tempers_boiling_over_blue_jays_cool_off_the_royals_sports',
]