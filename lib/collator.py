#!/usr/bin/env python
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.corpus import stopwords
from sklearn.metrics.pairwise import cosine_similarity


class Collator(object):
    NGRAM_RANGE = (1, 3)
    ROUND_TO = 4

    def __init__(self, ngram_range=NGRAM_RANGE):
        self.vectorizer = self._get_vectorizer(ngram_range)

    def _get_vectorizer(self, ngram_range):
        ignore_words = stopwords.words('english')
        vectorizer = TfidfVectorizer(stop_words=ignore_words, ngram_range=ngram_range)

        return vectorizer

    def _round_correlation(self, correlation_vector):
        return [round(correlation, self.ROUND_TO) for correlation in correlation_vector]

    def get_tfidf_matrix(self, docs):
        """
        Takes in 2 or more documents and creates a Tf-idf-weighted document-term matrix
        :param docs: A list of documents, where each documents is a string
        extracted. All values of n such that min_n <= n <= max_n will be used
        :returns tfidf_matrix: Tf-idf-weighted document-term matrix
        """
        tfidf_matrix = self.vectorizer.fit_transform(docs)

        return tfidf_matrix

    def transform_fresh_data(self, fresh_data):
        fresh_tfidf_matrix = self.vectorizer.transform(fresh_data)

        return fresh_tfidf_matrix

    def cosine_similarities_matrix(self, tfidf_matrix):
        """
        Computes cosine similarity matrix from tfidf_matrix
        :param tfidf_matrix: Tf-idf-weighted document-term matrix
        :returns cos_similarities_matrix: cosine similarity matrix
        """
        cos_similarities_matrix = []
        for row in tfidf_matrix:
            cosine_similarities_row = cosine_similarity(row, tfidf_matrix).flatten()
            cosine_similarities_row = self._round_correlation(cosine_similarities_row)
            cos_similarities_matrix.append(cosine_similarities_row)

        return cos_similarities_matrix

    def cosine_similarity_value(self, vec1, vec2):
        """
        Computes cosine similarity between 2 vectors or a vector and rest of the tfidf_matrix, 
        if doc_num2 is not provided
        :param tfidf_matrix: Tf-idf-weighted document-term matrix
        :param doc_num1: Position of the document when it was passed to compute tfidf_matrix
        :param doc_num2: Position of the document when it was passed to compute tfidf_matrix
        :returns cos_similarity: cosine similarity between doc_num1 & doc_num2 vectors
        """
        cos_similarity = cosine_similarity(vec1, vec2)[0]
        cos_similarity = self._round_correlation(cos_similarity)

        return cos_similarity


def grabData(filename):
    out_file = open(filename)
    data = out_file.read()
    out_file.close()

    return data


if __name__ == '__main__':
    import argument_parser
    import articles

    parsed_args = argument_parser.parse_arguments()

    ngram_range = parsed_args.ngram_range
    collator = Collator(ngram_range=ngram_range)
    all_article_names = articles.EDITED_SAME_SOURCE + articles.SAME_ARTICLE_DIFFERENT_SOURCE \
                        + articles.SAME_CATEGORY + articles.RANDOM
    all_article_names = ['input/{0}'.format(article_name) for article_name in all_article_names]
    docs = []
    for article in all_article_names:
        doc = grabData(article)
        docs.append(doc)
    tfidf_matrix = collator.get_tfidf_matrix(docs)

    # print out rows of cosine similarty
    similarities_matrix = collator.cosine_similarities_matrix(tfidf_matrix)
    matrix_range = parsed_args.matrix_range
    first_row = matrix_range[0] if matrix_range else 1
    last_row = matrix_range[1]  if matrix_range else len(similarities_matrix)
    for idx, row in enumerate(similarities_matrix):
        row = ["{0:.4f}".format(correlation) for correlation in row]
        row_num = idx + 1
        if first_row <= row_num <= last_row:
            print row[first_row -1 : last_row]

    # print out correlation between 2 or more documents
    similarity_docs = parsed_args.similarity
    if similarity_docs:
        doc_num1 = similarity_docs[0]
        vec1 = tfidf_matrix[doc_num1 - 1]
        vec2 = tfidf_matrix
        if len(similarity_docs) == 2:
            doc_num2 = similarity_docs[1]
            vec2 = tfidf_matrix[doc_num2 - 1]
        print collator.cosine_similarity_value(vec1, vec2)