from setuptools import setup


setup(
    name='collator',
    author='Gurlaik Pannu',
    author_email='gurlaik.pannu@gmail.com',
    url='https://bitbucket.org/gurlaikPannu/collator',
    description='TF-IDF implementation along with Cosine Similarity to match text documents',
    license='none',
    platforms='all',
    install_requires=[
        'numpy>=1.8.2', 
        'scipy>=0.14.0', 
        'scikit-learn>=0.15.1',
        'nltk>=3.0.4'
    ],
)
